var express = require('express');
require('express-async-errors');
var router = express.Router();
const connection = require('../config/database_globalwebapp');
const TblMasterPP = require('../models/tbl_master_pp');
const TblUser = require('../models/tbl_user');
const global_function = require('../config/function');

router.all('*', async(req, res, next) => {
  
  var token = global_function.check_null(req.token)

  var data = await TblUser.findAll({
    where: {
      api_token: token
    },
    attributes: ['id']
  })

  if(data.length == 0 || global_function.check_null(req.token) == '-') {
    res.status(401).send({'httpStatus': 401, 'message': 'unauthorized access or token is invalid', 'data': null});
  } else {
    next();
  }

  
})

router.get('/getSummaryDashboard', async(req, res, next) => {

  var {aproval_flag, id_company, id_dept, name, email} = req.query;
  
  /* EPP Summary */
  // EPP Submission
  if(aproval_flag != 0) {
    var query_epp_submission = await TblMasterPP.count({
      where: {
        status: 1,
        status_send_aprove: 0,
        id_company: id_company
      },
      attributes: ['id_master']
    })
  } else {
    var query_epp_submission = await TblMasterPP.count({
      where: {
        status: 1,
        status_send_aprove: 0,
        id_company: id_company,
        id_dept: id_dept,
        user_submission: name
      },
      attributes: ['id_master']
    })
  }

  // EPP Pending Approval
  if(aproval_flag != 0) {
    var query_epp_pending_approval = await TblMasterPP.count({
      where: {
        status: 1,
        status_send_aprove: 1,
        id_company: id_company
      },
      attributes: ['id_master']
    })
  } else {
    var query_epp_pending_approval = await TblMasterPP.count({
      where: {
        status: 1,
        status_send_aprove: 1,
        id_company: id_company,
        id_dept: id_dept,
        user_submission: name
      },
      attributes: ['id_master']
    })
  }

  // EPP Rejected
  if(aproval_flag != 0) {
    var query_epp_rejected = await TblMasterPP.count({
      where: {
        status: 1,
        status_send_aprove: -1,
        id_company: id_company
      },
      attributes: ['id_master']
    })
  } else {
    var query_epp_rejected = await TblMasterPP.count({
      where: {
        status: 1,
        status_send_aprove: -1,
        id_company: id_company,
        id_dept: id_dept,
        user_submission: name
      },
      attributes: ['id_master']
    })
  }

  // EPP Approved
  if(aproval_flag != 0) {
    var query_epp_approved = await TblMasterPP.count({
      where: {
        status: 1,
        aprove_bod: 1,
        id_company: id_company
      },
      attributes: ['id_master']
    })
  } else {
    var query_epp_approved = await TblMasterPP.count({
      where: {
        status: 1,
        aprove_bod: 1,
        id_company: id_company,
        id_dept: id_dept,
        user_submission: name
      },
      attributes: ['id_master']
    })
  }

  // EPP All
  if(aproval_flag != 0) {
    var query_epp_all = await TblMasterPP.count({
      where: {
        status: 1,
        id_company: id_company
      },
      attributes: ['id_master']
    })
  } else {
    var query_epp_all = await TblMasterPP.count({
      where: {
        status: 1,
        id_company: id_company,
        id_dept: id_dept,
        user_submission: name
      },
      attributes: ['id_master']
    })
  }
  /* EPP Summary */

  /* Demo Car Summary */
  // Demo Car Submission
  if(aproval_flag != 0) {
    var query_democar_submission = "SELECT id_req_democar_group FROM qv_req_demo_car" 
    + " WHERE send_approval = 0 AND sign_1 = '0' AND sign_2 = '0' AND id_company = $id_company ";

  } else {
    var query_democar_submission = "SELECT id_req_democar_group FROM qv_req_demo_car" 
    + " WHERE send_approval = 0 AND sign_1 = '0' AND sign_2 = '0' AND id_company = $id_company AND submission_email = $email ";
  }

  var data_democar_submission = await connection.query(query_democar_submission, {
    bind: {id_company: id_company, email: email }, 
    //attributes: { exclude: ["password"] },
    type: connection.QueryTypes.SELECT
  });

  // Demo Car Pending Approval
  if(aproval_flag != 0) {
    var query_democar_pending_approval = "SELECT id_req_democar_group FROM qv_req_demo_car" 
    + " WHERE send_approval = 1 AND sign_1 = '0' OR send_approval = 1 AND sign_2 = '0' AND id_company = $id_company ";
  } else {
    var query_democar_pending_approval = "SELECT id_req_democar_group FROM qv_req_demo_car" 
    + " WHERE send_approval = 1 AND sign_1 = '0' OR send_approval = 1 AND sign_2 = '0' AND id_company = $id_company AND submission_email = $email ";
  }

  var data_democar_pending_approval = await connection.query(query_democar_pending_approval, {
    bind: {id_company: id_company, email: email }, 
    //attributes: { exclude: ["password"] },
    type: connection.QueryTypes.SELECT
  });

  // Demo Car Rejected
  if(aproval_flag != 0) {
    var query_democar_rejected = "SELECT id_req_democar_group FROM qv_req_demo_car" 
    + " WHERE send_approval = 2 AND sign_1 = '2' OR send_approval = 2 AND sign_2 = '2' AND id_company = $id_company ";
  } else {
    var query_democar_rejected = "SELECT id_req_democar_group FROM qv_req_demo_car" 
    + " WHERE send_approval = 2 AND sign_1 = '2' OR send_approval = 2 AND sign_2 = '2' AND id_company = $id_company AND submission_email = $email";
  
  }

  var data_democar_rejected = await connection.query(query_democar_rejected, {
    bind: {id_company: id_company, email: email }, 
    //attributes: { exclude: ["password"] },
    type: connection.QueryTypes.SELECT
  });

  // Demo Car Approved
  if(aproval_flag != 0) {
    var query_democar_approved = "SELECT id_req_democar_group FROM qv_req_demo_car" 
    + " WHERE send_approval = 2 AND sign_1 = '1' AND sign_2 = '1' AND id_company = $id_company ";
  } else {
    var query_democar_approved = "SELECT id_req_democar_group FROM qv_req_demo_car" 
    + " WHERE send_approval = 2 AND sign_1 = '1' AND sign_2 = '1' AND id_company = $id_company AND submission_email = $email";
  }

  var data_democar_approved = await connection.query(query_democar_approved, {
    bind: {id_company: id_company, email: email }, 
    //attributes: { exclude: ["password"] },
    type: connection.QueryTypes.SELECT
  });

  // Demo Car All
  if(aproval_flag != 0) {
    var query_democar_all = "SELECT id_req_democar_group FROM qv_req_demo_car" 
    + " WHERE id_company = $id_company ";
  } else {
    var query_democar_all = "SELECT id_req_democar_group FROM qv_req_demo_car" 
    + " WHERE id_company = $id_company AND submission_email = $email";
  }

  var data_democar_all = await connection.query(query_democar_all, {
    bind: {id_company: id_company, email: email }, 
    //attributes: { exclude: ["password"] },
    type: connection.QueryTypes.SELECT
  });
  /* Demo Car Summary */

  /* Book Meeting Summary */
    // Book Meeting All
  if(aproval_flag != 0) {
    var query_book_meeting_all = "SELECT id_book FROM qv_room_book_meeting" 
    + " WHERE status = '1' AND id_company = $id_company ";
  } else {
    var query_book_meeting_all = "SELECT id_book FROM qv_room_book_meeting" 
    + " WHERE status = '1' AND id_company = $id_company AND id_dept = $id_dept AND user_submission = $name";
  }

  var data_book_meeting_all = await connection.query(query_book_meeting_all, {
    bind: {id_company: id_company, id_dept: id_dept, name: name }, 
    //attributes: { exclude: ["password"] },
    type: connection.QueryTypes.SELECT
  });
  /* Book Meeting Summary */

  /* SPK Summary */
  // SPK Mazda
  var query_spk_mazda = "SELECT id_trans_sales FROM qv_complite_master_sell WHERE nospk IS NOT NULL AND status_flag_sell = '2' AND status = '1'";

  var data_spk_mazda = await connection.query(query_spk_mazda, {
    type: connection.QueryTypes.SELECT
  });

  // SPK BMW
  var query_spk_bmw = "SELECT id_trans_sales FROM qv_complite_master_sell_bmw WHERE nospk IS NOT NULL AND status_flag_sell = '2' AND status = '1'";

  var data_spk_bmw = await connection.query(query_spk_bmw, {
    type: connection.QueryTypes.SELECT
  });

  // SPK Maserati
  var query_spk_mas = "SELECT id_trans_sales FROM qv_complite_master_sell_mas WHERE nospk IS NOT NULL AND status_flag_sell = '2' AND status = '1'";

  var data_spk_mas = await connection.query(query_spk_mas, {
    type: connection.QueryTypes.SELECT
  });
  /* SPK Summary */

  var merge_query = [query_epp_submission, query_epp_pending_approval, query_epp_rejected, query_epp_approved, query_epp_all, 
    data_democar_submission, data_democar_pending_approval, data_democar_rejected, data_democar_approved, data_democar_all,
    data_book_meeting_all,
    data_spk_mazda, data_spk_bmw, data_spk_mas] ;

  // gunakan promise kalau query select nya lebih dari 1
  Promise
    .all(merge_query)
    .then(response => {
        var callback = {
          'epp_submission': response[0], 
          'epp_pending_approval': response[1],
          'epp_rejected': response[2],
          'epp_approved': response[3],
          'epp_all': response[4],
          'democar_submission': response[5].length,
          'democar_pending_approval': response[6].length,
          'democar_rejected': response[7].length,
          'democar_approved': response[8].length,
          'democar_all': response[9].length,
          'book_meeting_all': response[10].length,
          'spk_mazda': response[11].length,
          'spk_bmw': response[12].length,
          'spk_mas': response[13].length
        };

        res.status(200).send({'httpStatus': 200, 'message': 'success', 'data': callback});
    })
    .catch(err => {
        res.status(500).send({'httpStatus': 500, 'message': err, 'data': null});
    });

});

module.exports = router;