var express = require('express');
require('express-async-errors');
var router = express.Router();
const connection = require('../config/database_globalwebapp');
const TblUser = require('../models/tbl_user');

// Panggil global function
const global_function = require('../config/function');

router.all('*', async(req, res, next) => {
  
  var token = global_function.check_null(req.token)

  var data = await TblUser.findAll({
    where: {
      api_token: token
    },
    attributes: ['id']
  })

  if(data.length == 0 || global_function.check_null(req.token) == '-') {
    res.status(401).send({'httpStatus': 401, 'message': 'unauthorized access or token is invalid', 'data': null});
  } else {
    next();
  }

  
})

router.get('/datatableSearch', async(req, res, next) => {

  var {id_company, id_dept, limit, start, search, order, dir, is_date_search, start_date, end_date, approve_head} = req.query
  console.log(req.query)
  if(!order) order = 'date_send_pr', dir = 'DESC';

  if(is_date_search == 'yes') {
    if(global_function.check_null_start(start) == '-') {
      var query = "SELECT * FROM qv_head_pp_complite"
      + " WHERE id_company = $id_company AND id_dept = $id_dept"
      + " AND flag_send_pr = '1'"
      + " AND status = '1'"
      + " AND date_send_pr >= $start_date "
      + " AND date_send_pr <= $end_date "
      + " AND aprove_head = $approve_head"
      + " ORDER BY "+order+" "+dir+" "
      + " LIMIT $limit";
    } else {
      var query = "SELECT * FROM qv_head_pp_complite"
      + " WHERE id_company = $id_company AND id_dept = $id_dept"
      + " AND flag_send_pr = '1'"
      + " AND status = '1'"
      + " AND date_send_pr >= $start_date "
      + " AND date_send_pr <= $end_date "
      + " AND aprove_head = $approve_head"
      + " ORDER BY "+order+" "+dir+" "
      + " LIMIT $limit, $start";
    }
  } else if(global_function.check_null(search) == '-') {

    if(global_function.check_null_start(start) == '-') {
      var query = "SELECT * FROM qv_head_pp_complite"
      + " WHERE id_company = $id_company AND id_dept = $id_dept"
      + " AND flag_send_pr = '1'"
      + " AND status = '1'"
      + " ORDER BY "+order+" "+dir+" "
      + " LIMIT $limit";
    } else {
      var query = "SELECT * FROM qv_head_pp_complite"
      + " WHERE id_company = $id_company AND id_dept = $id_dept"
      + " AND flag_send_pr = '1'"
      + " AND status = '1'"
      + " ORDER BY "+order+" "+dir+" "
      + " LIMIT $limit, $start";
    }
      
  } else {
    if(global_function.check_null_start(start) == '-') {

    var query = "SELECT * FROM qv_head_pp_complite" 
    + " WHERE "
    + " status = '1'AND id_company = $id_company AND id_dept = $id_dept AND flag_send_pr = '1' AND id_master like $search" 
    + " OR status = '1'AND id_company = $id_company AND id_dept = $id_dept AND flag_send_pr = '1' AND vendor like $search" 
    + " OR status = '1'AND id_company = $id_company AND id_dept = $id_dept AND flag_send_pr = '1' AND date_send_pr like $search" 
    + " OR status = '1'AND id_company = $id_company AND id_dept = $id_dept AND flag_send_pr = '1' AND type_purchase like $search"
    + " OR status = '1'AND id_company = $id_company AND id_dept = $id_dept AND flag_send_pr = '1' AND currency like $search"
    + " OR status = '1'AND id_company = $id_company AND id_dept = $id_dept AND flag_send_pr = '1' AND gran_total like $search"
    + " OR status = '1'AND id_company = $id_company AND id_dept = $id_dept AND flag_send_pr = '1' AND term_top like $search"
    + " ORDER BY "+order+" "+dir+" "
    + " LIMIT $limit";

    } else {
      var query = "SELECT * FROM qv_head_pp_complite" 
    + " WHERE "
    + " status = '1'AND id_company = $id_company AND id_dept = $id_dept AND flag_send_pr = '1' AND id_master like $search" 
    + " OR status = '1'AND id_company = $id_company AND id_dept = $id_dept AND flag_send_pr = '1' AND vendor like $search" 
    + " OR status = '1'AND id_company = $id_company AND id_dept = $id_dept AND flag_send_pr = '1' AND date_send_pr like $search" 
    + " OR status = '1'AND id_company = $id_company AND id_dept = $id_dept AND flag_send_pr = '1' AND type_purchase like $search"
    + " OR status = '1'AND id_company = $id_company AND id_dept = $id_dept AND flag_send_pr = '1' AND currency like $search"
    + " OR status = '1'AND id_company = $id_company AND id_dept = $id_dept AND flag_send_pr = '1' AND gran_total like $search"
    + " OR status = '1'AND id_company = $id_company AND id_dept = $id_dept AND flag_send_pr = '1' AND term_top like $search"
    + " ORDER BY "+order+" "+dir+" "
    + " LIMIT $limit, $start";

    }
    
      
  }

  search = global_function.check_date(search);
  start_date = global_function.check_date(start_date);
  end_date = global_function.check_date(end_date);

  var data = await connection.query(query, {
    bind: { status: '1', id_company: id_company, id_dept: id_dept, search: '%'+search+'%', limit: limit, start: start, start_date: start_date+' 00:00:00', end_date: end_date+' 00:00:00', approve_head: approve_head }, 
    //attributes: { exclude: ["password"] },
    type: connection.QueryTypes.SELECT
  });

  if(data[0]) {
      res.status(200).send({'httpStatus': 200, 'message': 'success', 'data': data, 'length': data.length});
    } else {  
      res.status(404).send({'httpStatus': 404, 'message': 'no data', 'data': data, 'length': 0});
  }
})

router.get('/countAllResult', async(req, res, next) => {

  var {id_company, id_dept } = req.query;

  var data = await connection.query("SELECT id_master FROM qv_head_pp_complite WHERE id_company = $id_company AND id_dept = $id_dept  AND status = $status AND flag_send_pr = '1'", {
      bind: { status: '1', id_company: id_company, id_dept: id_dept}, 
      //attributes: { exclude: ["password"] },
      type: connection.QueryTypes.SELECT
    });
  if(data[0]) {
      res.status(200).send({'httpStatus': 200, 'message': 'success', 'data': data, 'length': data.length});
    } else {  
      res.status(404).send({'httpStatus': 404, 'message': 'no data', 'data': data, 'length': data.length});
  }
})

module.exports = router;
