var express = require('express');
require('express-async-errors');
var router = express.Router();
const connection = require('../config/database_globalwebapp');

// Panggil Model
const TblUser = require('../models/tbl_user');

// Panggil global function
const global_function = require('../config/function');


//Panggil View
router.get('/', async(req, res, next) => {
    var data = await connection.query("SELECT * FROM qv_complite_user where status = $status", {
      bind: { status: '1' }, 
      attributes: { exclude: ["password"] },
      type: connection.QueryTypes.SELECT
    });
  
    if(data[0]) {
      res.status(200).send({'httpStatus': 200, 'message': 'success', 'data': data});
    } else {  
      res.status(404).send({'httpStatus': 404, 'message': 'no data', 'data': null});
    }
  
  });

router.post('/countAllResult', async(req, res, next) => {
    var data = await connection.query("SELECT id FROM qv_complite_user WHERE status = $status", {
        bind: { status: '1'}, 
        attributes: { exclude: ["password"] },
        type: connection.QueryTypes.SELECT
      });
    if(data[0]) {
        res.status(200).send({'httpStatus': 200, 'message': 'success', 'data': data.length});
      } else {  
        res.status(404).send({'httpStatus': 404, 'message': 'no data', 'data': data.length});
    }
})

router.post('/datatableSearch', async(req, res, next) => {

  var {limit, start, search, status, order, dir} = req.body

  if(!order) order = 'id', dir = 'ASC';

  if(global_function.check_null(search) == '-') {

    if(global_function.check_null_start(start) == '-') {
      var query = "SELECT * FROM qv_complite_user"
      + " WHERE status = '1'"
      + " ORDER BY "+order+" "+dir+" "
      + " LIMIT $limit";
    } else {
      var query = "SELECT * FROM qv_complite_user"
      + " WHERE status = '1'"
      + " ORDER BY "+order+" "+dir+" "
      + " LIMIT $limit, $start";
    }
      
  } else {
    if(global_function.check_null_start(start) == '-') {

    var query = "SELECT * FROM qv_complite_user" 
    + " WHERE "
    + " status = '1' AND id like $search" 
    + " OR status = '1' AND name like $search" 
    + " OR status = '1' AND username like $search" 
    + " OR status = '1' AND name_grp like $search"
    + " OR status = '1' AND company like $search" 
    + " OR status = '1' AND name_branch like $search" 
    + " OR status = '1' AND dept like $search"
    + " OR status = '1' AND email like $search"
    + " ORDER BY "+order+" "+dir+" "
    + " LIMIT $limit";

    } else {
      var query = "SELECT * FROM qv_complite_user" 
    + " WHERE "
    + " status = '1' AND id like $search" 
    + " OR status = '1' AND name like $search" 
    + " OR status = '1' AND username like $search" 
    + " OR status = '1' AND name_grp like $search"
    + " OR status = '1' AND company like $search" 
    + " OR status = '1' AND name_branch like $search" 
    + " OR status = '1' AND dept like $search"
    + " OR status = '1' AND email like $search"
    + " ORDER BY "+order+" "+dir+" "
    + " LIMIT $limit, $start";

    }
    
      
  }

  var data = await connection.query(query, {
    bind: { status: '1', search: '%'+search+'%', limit: limit, start: start }, 
    //attributes: { exclude: ["password"] },
    type: connection.QueryTypes.SELECT
  });

  if(data[0]) {
      res.status(200).send({'httpStatus': 200, 'message': 'success', 'data': data, 'length': data.length});
    } else {  
      res.status(404).send({'httpStatus': 404, 'message': 'no data', 'data': data, 'length': 0});
  }
})




module.exports = router;
