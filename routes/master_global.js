var express = require('express');
require('express-async-errors');
var router = express.Router();
const connection = require('../config/database_globalwebapp');

// Panggil Model
const TblUser = require('../models/tbl_user');
const TblUserGroups = require('../models/tbl_user_groups');
const TblCurr = require('../models/tbl_curr');
const TblVendor = require('../models/tbl_vendor');
const TblCoa= require('../models/tbl_coa');
const TblCompany = require('../models/tbl_company');
const TblBrand = require('../models/tbl_brand');
const TblBranches = require('../models/tbl_branches');
const TblDept = require('../models/tbl_dept');
const TblTypePurchase = require('../models/tbl_type_purchase');
const TblArea = require('../models/tbl_area');
const TblBank = require('../models/tbl_bank');
const TblChildMenu = require('../models/tbl_child_menu');
const TblCustomer = require('../models/tbl_customer');
const TblMenuUserGroup = require('../models/tbl_menu_user_group');
const TblParentMenu = require('../models/tbl_parent_menu');
const TblSubChildMenu = require('../models/tbl_sub_child_menu');
const TblStatusApp = require('../models/tbl_status_app');
const TblSales = require('../models/tbl_sales');



// Panggil global function
const global_function = require('../config/function');

router.get('/getUser', async(req, res, next) => {

    var data = await TblUser.findAll({
      where: {  status: '1' }
    })
    
    if(data[0]) {
        res.status(200).send({'httpStatus': 200, 'message': 'success', 'data': data, 'length': data.length});
      } else {  
        res.status(404).send({'httpStatus': 404, 'message': 'no data', 'data': null, 'length': 0});
    }
  
  })

router.get('/getUserGroups', async(req, res, next) => {

    var data = await TblUserGroups.findAll({
      where: {  status: '1' }
    })
    
    if(data[0]) {
        res.status(200).send({'httpStatus': 200, 'message': 'success', 'data': data, 'length': data.length});
      } else {  
        res.status(404).send({'httpStatus': 404, 'message': 'no data', 'data': null, 'length': 0});
    }
  
  })

router.get('/getCurrency', async(req, res, next) => {

    var data = await TblCurr.findAll({
      where: {  status: '1' }
    })
    
    if(data[0]) {
        res.status(200).send({'httpStatus': 200, 'message': 'success', 'data': data, 'length': data.length});
      } else {  
        res.status(404).send({'httpStatus': 404, 'message': 'no data', 'data': null, 'length': 0});
    }
  
  })

router.get('/getVendor', async(req, res, next) => {
    
    var {id_company, id_dept} = req.query;
  
    var data = await TblVendor.findAll({
      where: { id_company: id_company, id_dept: id_dept, status: '1' }
    })
    
    if(data[0]) {
        res.status(200).send({'httpStatus': 200, 'message': 'success', 'data': data, 'length': data.length});
      } else {  
        res.status(404).send({'httpStatus': 404, 'message': 'no data', 'data': null, 'length': 0});
    }
  
  })


router.get('/getCoa', async(req, res, next) => {

    var {id_company} = req.query;
  
    var data = await TblCoa.findAll({
      where: {  id_company: id_company, status: '1' }
    })
    
    if(data[0]) {
        res.status(200).send({'httpStatus': 200, 'message': 'success', 'data': data, 'length': data.length});
      } else {  
        res.status(404).send({'httpStatus': 404, 'message': 'no data', 'data': null, 'length': 0});
    }
  
  })


router.get('/getCompany', async(req, res, next) => {

    var data = await TblCompany.findAll({
      where: {  status: '1' }
    })
    
    if(data[0]) {
        res.status(200).send({'httpStatus': 200, 'message': 'success', 'data': data, 'length': data.length});
      } else {  
        res.status(404).send({'httpStatus': 404, 'message': 'no data', 'data': null, 'length': 0});
    }
  
  })

router.get('/getBrand', async(req, res, next) => {
  
    var data = await TblBrand.findAll({
      where: {  status: '1' }
    })
    
    if(data[0]) {
        res.status(200).send({'httpStatus': 200, 'message': 'success', 'data': data, 'length': data.length});
      } else {  
        res.status(404).send({'httpStatus': 404, 'message': 'no data', 'data': null, 'length': 0});
    }
  
  })

router.get('/getBranches', async(req, res, next) => {

    var {id_company} = req.query;
  
    var data = await TblBranches.findAll({
      where: {  id_company: id_company, status: '1' }
    })
    
    if(data[0]) {
        res.status(200).send({'httpStatus': 200, 'message': 'success', 'data': data, 'length': data.length});
      } else {  
        res.status(404).send({'httpStatus': 404, 'message': 'no data', 'data': null, 'length': 0});
    }
  
  })

router.get('/getDept', async(req, res, next) => {
  
    var data = await TblDept.findAll({
      where: {  status: '1' }
    })
    
    if(data[0]) {
        res.status(200).send({'httpStatus': 200, 'message': 'success', 'data': data, 'length': data.length});
      } else {  
        res.status(404).send({'httpStatus': 404, 'message': 'no data', 'data': null, 'length': 0});
    }
  
  })

router.get('/getTypePurchase', async(req, res, next) => {

    var data = await TblTypePurchase.findAll()
      
    if(data[0]) {
        res.status(200).send({'httpStatus': 200, 'message': 'success', 'data': data, 'length': data.length});
      } else {  
        res.status(404).send({'httpStatus': 404, 'message': 'no data', 'data': null, 'length': 0});
    }
    
  })

router.get('/getArea', async(req, res, next) => {

    var data = await TblArea.findAll({
      where: {  status: '1' }
    })
      
    if(data[0]) {
        res.status(200).send({'httpStatus': 200, 'message': 'success', 'data': data, 'length': data.length});
      } else {  
        res.status(404).send({'httpStatus': 404, 'message': 'no data', 'data': null, 'length': 0});
      }
    
  })

router.get('/getBank', async(req, res, next) => {

    var data = await TblBank.findAll({
      where: {  status: '1' }
    })
      
    if(data[0]) {
        res.status(200).send({'httpStatus': 200, 'message': 'success', 'data': data, 'length': data.length});
      } else {  
        res.status(404).send({'httpStatus': 404, 'message': 'no data', 'data': null, 'length': 0});
      }
    
  })

router.get('/getChildMenu', async(req, res, next) => {

    var data = await TblChildMenu.findAll()
      
    if(data[0]) {
        res.status(200).send({'httpStatus': 200, 'message': 'success', 'data': data, 'length': data.length});
      } else {  
        res.status(404).send({'httpStatus': 404, 'message': 'no data', 'data': null, 'length': 0});
      }
    
  })

router.get('/getCustomer', async(req, res, next) => {
  
    var data = await TblCustomer.findAll({
      where: {  status: '1' }
    })
    
    if(data[0]) {
        res.status(200).send({'httpStatus': 200, 'message': 'success', 'data': data, 'length': data.length});
      } else {  
        res.status(404).send({'httpStatus': 404, 'message': 'no data', 'data': null, 'length': 0});
    }
  
  })

router.get('/getMenuUserGroup', async(req, res, next) => {
  
    var data = await TblMenuUserGroup.findAll({
      where: {  status: '1' }
    })
    
    if(data[0]) {
        res.status(200).send({'httpStatus': 200, 'message': 'success', 'data': data, 'length': data.length});
      } else {  
        res.status(404).send({'httpStatus': 404, 'message': 'no data', 'data': null, 'length': 0});
    }
  
  })

router.get('/getParentMenu', async(req, res, next) => {
  
    var data = await TblParentMenu.findAll({
      where: {  status_del: '0' }
    })
    
    if(data[0]) {
        res.status(200).send({'httpStatus': 200, 'message': 'success', 'data': data, 'length': data.length});
      } else {  
        res.status(404).send({'httpStatus': 404, 'message': 'no data', 'data': null, 'length': 0});
    }
  
  })

router.get('/getSubChildMenu', async(req, res, next) => {

    var data = await TblSubChildMenu.findAll()
      
    if(data[0]) {
        res.status(200).send({'httpStatus': 200, 'message': 'success', 'data': data, 'length': data.length});
      } else {  
        res.status(404).send({'httpStatus': 404, 'message': 'no data', 'data': null, 'length': 0});
      }
    
  })

router.get('/getStatusApp', async(req, res, next) => {
  
    var data = await TblStatusApp.findAll({
      where: {  status: '1' }
    })
    
    if(data[0]) {
        res.status(200).send({'httpStatus': 200, 'message': 'success', 'data': data, 'length': data.length});
      } else {  
        res.status(404).send({'httpStatus': 404, 'message': 'no data', 'data': null, 'length': 0});
    }
  
  })

router.get('/getSales', async(req, res, next) => {
  
    var data = await TblSales.findAll({
      where: {  status: '1' }
    })
    
    if(data[0]) {
        res.status(200).send({'httpStatus': 200, 'message': 'success', 'data': data, 'length': data.length});
      } else {  
        res.status(404).send({'httpStatus': 404, 'message': 'no data', 'data': null, 'length': 0});
    }
  
  })

router.get('/getUser/:id', async(req, res, next) => {
    var {id} = req.params;
    var data = await TblUser.findOne(
      {
        where: { id: id, status: '1' },
        attributes: { exclude: ["password"] }
      });
  
    if(data) {
      res.status(200).send({'httpStatus': 200, 'message': 'success', 'data': data});
    } else {  
      res.status(404).send({'httpStatus': 404, 'message': 'no data', 'data': null});
    }
  
  })

router.get('/getUserGroups/:id', async(req, res, next) => {
    var {id} = req.params;
    var data = await TblUserGroups.findOne(
      {
        where: { id_group: id, status: '1' }
      });
  
    if(data) {
      res.status(200).send({'httpStatus': 200, 'message': 'success', 'data': data});
    } else {  
      res.status(404).send({'httpStatus': 404, 'message': 'no data', 'data': null});
    }
  
  })

router.get('/getCurrency/:id', async(req, res, next) => {
    var {id} = req.params;
    var data = await TblCurr.findOne(
      {
        where: { id_curr: id, status: '1' }
      });
  
    if(data) {
      res.status(200).send({'httpStatus': 200, 'message': 'success', 'data': data});
    } else {  
      res.status(404).send({'httpStatus': 404, 'message': 'no data', 'data': null});
    }
  
  })

router.get('/getVendor/:id_company/:id_dept/:id', async(req, res, next) => {
    
    var {id_company, id_dept, id} = req.params;
  
    var data = await TblVendor.findAll({
      where: { id_company: id_company, id_dept: id_dept, id_vendor: id, status: '1' }
    })
    
    if(data[0]) {
        res.status(200).send({'httpStatus': 200, 'message': 'success', 'data': data, 'length': data.length});
      } else {  
        res.status(404).send({'httpStatus': 404, 'message': 'no data', 'data': null, 'length': 0});
    }
  
  })

router.get('/getCoa/:id_company/:id', async(req, res, next) => {

    var {id_company, id} = req.params;
  
    var data = await TblCoa.findAll({
      where: {  id_company: id_company, id_coa: id, status: '1' }
    })
    
    if(data[0]) {
        res.status(200).send({'httpStatus': 200, 'message': 'success', 'data': data, 'length': data.length});
      } else {  
        res.status(404).send({'httpStatus': 404, 'message': 'no data', 'data': null, 'length': 0});
    }
  
  })

router.get('/getCompany/:id', async(req, res, next) => {

    var {id} = req.params;

    var data = await TblCompany.findAll({
      where: { id_company: id, status: '1' }
    })
    
    if(data[0]) {
        res.status(200).send({'httpStatus': 200, 'message': 'success', 'data': data, 'length': data.length});
      } else {  
        res.status(404).send({'httpStatus': 404, 'message': 'no data', 'data': null, 'length': 0});
    }
  
  })

router.get('/getBrand/:id', async(req, res, next) => {

    var {id} = req.params;
  
    var data = await TblBrand.findAll({
      where: { id_brand: id, status: '1' }
    })
      
    if(data[0]) {
        res.status(200).send({'httpStatus': 200, 'message': 'success', 'data': data, 'length': data.length});
      } else {  
        res.status(404).send({'httpStatus': 404, 'message': 'no data', 'data': null, 'length': 0});
    }
    
  })

router.get('/getBranches/:id_company/:id', async(req, res, next) => {

    var {id_company, id} = req.params;
    
    var data = await TblBranches.findAll({
      where: {  id_company: id_company, branch_id: id, status: '1' }
    })
        
    if(data[0]) {
        res.status(200).send({'httpStatus': 200, 'message': 'success', 'data': data, 'length': data.length});
      } else {  
        res.status(404).send({'httpStatus': 404, 'message': 'no data', 'data': null, 'length': 0});
    }
      
  })  
 
router.get('/getDept/:id', async(req, res, next) => {

    var {id} = req.params;
      
    var data = await TblDept.findAll({
      where: { id_dept: id, status: '1' }
    })
          
    if(data[0]) {
        res.status(200).send({'httpStatus': 200, 'message': 'success', 'data': data, 'length': data.length});
      } else {  
        res.status(404).send({'httpStatus': 404, 'message': 'no data', 'data': null, 'length': 0});
    }
        
  })

router.get('/getTypePurchase/:id', async(req, res, next) => {

    var {id} = req.params;
      
    var data = await TblTypePurchase.findAll({
      where: { id_flagpur: id }
    })
          
    if(data[0]) {
        res.status(200).send({'httpStatus': 200, 'message': 'success', 'data': data, 'length': data.length});
      } else {  
        res.status(404).send({'httpStatus': 404, 'message': 'no data', 'data': null, 'length': 0});
    }
        
  })

router.get('/getArea/:id', async(req, res, next) => {

    var {id} = req.params;
      
    var data = await TblArea.findAll({
      where: { area_id: id, status: '1' }
    })
          
    if(data[0]) {
        res.status(200).send({'httpStatus': 200, 'message': 'success', 'data': data, 'length': data.length});
      } else {  
        res.status(404).send({'httpStatus': 404, 'message': 'no data', 'data': null, 'length': 0});
    }
        
  })

router.get('/getBank/:id', async(req, res, next) => {

    var {id} = req.params;
      
    var data = await TblBank.findAll({
      where: { id_bank: id, status: '1' }
    })
          
    if(data[0]) {
        res.status(200).send({'httpStatus': 200, 'message': 'success', 'data': data, 'length': data.length});
      } else {  
        res.status(404).send({'httpStatus': 404, 'message': 'no data', 'data': null, 'length': 0});
    }
        
  })

router.get('/getChildMenu/:id', async(req, res, next) => {

    var {id} = req.params;
      
    var data = await TblChildMenu.findAll({
      where: { id_child: id }
    })
          
    if(data[0]) {
        res.status(200).send({'httpStatus': 200, 'message': 'success', 'data': data, 'length': data.length});
      } else {  
        res.status(404).send({'httpStatus': 404, 'message': 'no data', 'data': null, 'length': 0});
    }
        
  })

router.get('/getCustomer/:id', async(req, res, next) => {

    var {id} = req.params;
      
    var data = await TblCustomer.findAll({
      where: { id_cust: id, status: '1' }
    })
          
    if(data[0]) {
        res.status(200).send({'httpStatus': 200, 'message': 'success', 'data': data, 'length': data.length});
      } else {  
        res.status(404).send({'httpStatus': 404, 'message': 'no data', 'data': null, 'length': 0});
    }
        
  })

router.get('/getMenuUserGroup/:id', async(req, res, next) => {

    var {id} = req.params;
      
    var data = await TblMenuUserGroup.findAll({
      where: { id_menu_user_group: id, status: '1' }
    })
          
    if(data[0]) {
        res.status(200).send({'httpStatus': 200, 'message': 'success', 'data': data, 'length': data.length});
      } else {  
        res.status(404).send({'httpStatus': 404, 'message': 'no data', 'data': null, 'length': 0});
    }
        
  })

router.get('/getParentMenu/:id', async(req, res, next) => {

    var {id} = req.params;
      
    var data = await TblParentMenu.findAll({
      where: { id_parent: id, status_del: '0' }
    })
          
    if(data[0]) {
        res.status(200).send({'httpStatus': 200, 'message': 'success', 'data': data, 'length': data.length});
      } else {  
        res.status(404).send({'httpStatus': 404, 'message': 'no data', 'data': null, 'length': 0});
    }
        
  })

router.get('/getSubChildMenu/:id', async(req, res, next) => {

    var {id} = req.params;
      
    var data = await TblSubChildMenu.findAll({
      where: { id_subchild: id }
    })
          
    if(data[0]) {
        res.status(200).send({'httpStatus': 200, 'message': 'success', 'data': data, 'length': data.length});
      } else {  
        res.status(404).send({'httpStatus': 404, 'message': 'no data', 'data': null, 'length': 0});
    }
        
  })

router.get('/getStatusApp/:id', async(req, res, next) => {

    var {id} = req.params;
      
    var data = await TblStatusApp.findAll({
      where: { id_status_app: id, status: '1' }
    })
          
    if(data[0]) {
        res.status(200).send({'httpStatus': 200, 'message': 'success', 'data': data, 'length': data.length});
      } else {  
        res.status(404).send({'httpStatus': 404, 'message': 'no data', 'data': null, 'length': 0});
    }
        
  })

router.get('/getSales/:id', async(req, res, next) => {

    var {id} = req.params;
      
    var data = await TblSales.findAll({
      where: { id_sales: id, status: '1' }
    })
          
    if(data[0]) {
        res.status(200).send({'httpStatus': 200, 'message': 'success', 'data': data, 'length': data.length});
      } else {  
        res.status(404).send({'httpStatus': 404, 'message': 'no data', 'data': null, 'length': 0});
    }
        
  })

router.put('/updateUser/:id', async(req, res, next) => {

    var {id} = req.body;
  
    var update = await TblUser.update(req.body, {
      where: {
        id: id
      },
        returning: true,
        plain: true
    })
    
    if(update) {
        res.status(200).send({'httpStatus': 200, 'message': 'success', 'data': update});
      } else {  
        res.status(400).send({'httpStatus': 400, 'message': 'update master pp fail', 'data': null});
    }
  
  })

router.put('/updateUserGroups', async(req, res, next) => {

    var {id} = req.body;
  
    var update = await TblUserGroups.update(req.body, {
      where: {
        id_group: id
      },
        returning: true,
        plain: true
    })
    
    if(update) {
        res.status(200).send({'httpStatus': 200, 'message': 'success', 'data': update});
      } else {  
        res.status(400).send({'httpStatus': 400, 'message': 'update master pp fail', 'data': null});
    }
  
  })

router.put('/updateCurrency', async(req, res, next) => {

    var {id} = req.body;
  
    var update = await TblCurr.update(req.body, {
      where: {
        id_curr: id
      },
        returning: true,
        plain: true
    })
    
    if(update) {
        res.status(200).send({'httpStatus': 200, 'message': 'success', 'data': update});
      } else {  
        res.status(400).send({'httpStatus': 400, 'message': 'update master pp fail', 'data': null});
    }
  
  })


module.exports = router;
  