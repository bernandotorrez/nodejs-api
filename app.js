var express = require('express');
var path = require('path');
var cookieParser = require('cookie-parser');
var logger = require('morgan');
var createError = require('http-errors');
var helmet = require('helmet');
var winston = require('winston');
var global_function = require('./config/function');
var bearerToken = require('express-bearer-token');

// setiap membuat file router baru, silahkan panggil disini
var loginRouter = require('./routes/login');
var prFormRouter = require('./routes/pr_form');
var mainMenuRouter = require('./routes/main_menu');
var masterUserRouter = require('./routes/master_user');
var masterGlobalRouter = require('./routes/master_global');
var approvalPR = require('./routes/approval_pr');

var app = express();

app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: true }));
app.use(cookieParser());
app.use(bearerToken());
app.use(express.static(path.join(__dirname, 'public')));

// app.use(function(req, res, next) {
//     res.header("Access-Control-Allow-Origin", "*");
//     res.header('Access-Control-Allow-Methods', 'GET, POST, PUT, DELETE');
//     res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
//     next();
// });

// wajib saat naik ke production
if(process.env.NODE_ENV=='production'){
    app.use(helmet());
}

// setiap ada penambahan Router, inisialisasi index nya disini
app.use('/login', loginRouter);
app.use('/PRForm', prFormRouter);
app.use('/mainMenu', mainMenuRouter);
app.use('/masterUser', masterUserRouter);
app.use('/masterGlobal', masterGlobalRouter);
app.use('/approvalPR', approvalPR);
  
// catch 404 and forward to error handler
app.use(function(req, res, next) {
    next(createError(404));
  });

  // error handler
app.use(function(err, req, res, next) {
    // set locals, only providing error in development
    res.locals.message = err.message;
    res.locals.error = req.app.get('env').trim() == 'development' ? err : {};

    const logFile = winston.createLogger({
      level: 'info',
      format: winston.format.json(),
      defaultMeta: { service: 'user-service' },
      transports: [
        //
        // - Write to all logs with level `info` and below to `combined.log` 
        // - Write all logs error (and below) to `error.log`.
        //
        new winston.transports.File({ filename: 'error.log', level: 'error' }),
        new winston.transports.File({ filename: 'combined.log' })
      ]
    });

    logFile.log({
      level: 'error',
      message: `${err}`,
      httpStatus: `${err.status || 500}`,
      ip: `${req.ip}`,
      url: `${req.originalUrl}`,
      method: `${req.method}`,
      api_token: `${req.token}`,
      timestamp : global_function.log_time()
    });

    // render the error page
    res.status(err.status || 500).send({'httpStatus': err.status || 500, 'message': err.message, 'data': null});
    //res.send(err.message);
    //res.send({'httpStatus': 404, 'message': 'no data', 'data': null});
  });
  

module.exports = app;
