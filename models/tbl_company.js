const Sequelize = require('sequelize');
const connection = require('../config/database_globalwebapp');


  const tbl_company = connection.define('tbl_company', {
    id_company: {
      type: Sequelize.STRING,
      primaryKey: true
    },
    company: {
        type: Sequelize.STRING
    },
    short: {
      type: Sequelize.STRING
    },
    status: {
        type: Sequelize.STRING
    },
    flag_com_inv_receipt: {
        type: Sequelize.STRING
    },
    date_create: {
        type: Sequelize.STRING
    },
    date_update: {
        type: Sequelize.STRING
    },
    remarks: {
      type: Sequelize.STRING
    },
  },{
    freezeTableName: true,
    timestamps: false
  }
  
  );

  module.exports = tbl_company;
