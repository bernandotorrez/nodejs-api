const Sequelize = require('sequelize');
const connection = require('../config/database_globalwebapp');

const tbl_curr = connection.define('tbl_curr', {
    id_curr: {
      type: Sequelize.STRING,
      primaryKey: true
    },
    currency: {
        type: Sequelize.STRING,
    },
    status: {
        type: Sequelize.STRING,
    },
    date_create: {
        type: Sequelize.STRING,
    },
    date_update: {
        type: Sequelize.STRING,
    },
    remarks: {
        type: Sequelize.STRING,
    },
    mention: {
        type: Sequelize.STRING,
    },

  },{
    freezeTableName: true,
    timestamps: false
  }
  
  );

  module.exports = tbl_curr;
