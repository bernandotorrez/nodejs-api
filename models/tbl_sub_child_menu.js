const Sequelize = require('sequelize');
const connection = require('../config/database_globalwebapp');


  const tbl_sub_child_menu = connection.define('tbl_sub_child_menu', {
    id_subchild: {
      type: Sequelize.STRING,
      primaryKey: true
    },
    id_child: {
        type: Sequelize.STRING
    },
    order_subcild: {
      type: Sequelize.STRING
    },
    desc_sub: {
        type: Sequelize.STRING
    },
    anchor_sub_url: {
        type: Sequelize.STRING
    },
    icon_sub_child: {
        type: Sequelize.STRING
    },
    date_create: {
        type: Sequelize.STRING
    },
    date_update: {
        type: Sequelize.STRING
    },
    remarks: {
        type: Sequelize.STRING
    },
  },{
    freezeTableName: true,
    timestamps: false
  }
  
  );

  module.exports = tbl_sub_child_menu;
