const Sequelize = require('sequelize');
const connection = require('../config/database_globalwebapp');


  const tbl_status_app = connection.define('tbl_status_app', {
    id_status_app: {
      type: Sequelize.STRING,
      primaryKey: true
    },
    flag_app: {
        type: Sequelize.STRING
    },
    name_status_app: {
        type: Sequelize.STRING
    },
    under_company: {
        type: Sequelize.STRING
    },
    status: {
        type: Sequelize.STRING
    },
  },{
    freezeTableName: true,
    timestamps: false
  }
  
  );

  module.exports = tbl_status_app;
