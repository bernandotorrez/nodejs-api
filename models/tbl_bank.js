const Sequelize = require('sequelize');
const connection = require('../config/database_globalwebapp');


  const tbl_bank = connection.define('tbl_bank', {
    id_bank: {
      type: Sequelize.STRING,
      primaryKey: true
    },
    name_bank: {
        type: Sequelize.STRING
    },
    status: {
        type: Sequelize.STRING
    },
    date_create: {
        type: Sequelize.STRING
    },
    date_update: {
        type: Sequelize.STRING
    },
    remarks: {
        type: Sequelize.STRING
    },
  },{
    freezeTableName: true,
    timestamps: false
  }
  
  );

  module.exports = tbl_bank;
