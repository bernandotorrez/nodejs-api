const Sequelize = require('sequelize');
const connection = require('../config/database_globalwebapp');

const tbl_type_purchase = connection.define('tbl_type_purchase', {
    id_flagpur: {
      type: Sequelize.STRING,
      primaryKey: true
    },
    flag: {
        type: Sequelize.STRING,
    },
    type_purchase: {
        type: Sequelize.STRING,
    },

  },{
    freezeTableName: true,
    timestamps: false
  }
  
  );

  module.exports = tbl_type_purchase;
