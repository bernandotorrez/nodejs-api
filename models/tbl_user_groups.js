const Sequelize = require('sequelize');
const connection = require('../config/database_globalwebapp');


  const tbl_user_groups = connection.define('tbl_user_groups', {
    id_group: {
      type: Sequelize.STRING,
      primaryKey: true
    },
    name_grp: {
        type: Sequelize.STRING
    },
    apps_access: {
      type: Sequelize.STRING
    },
    status: {
        type: Sequelize.STRING
    },
    date_create: {
        type: Sequelize.STRING
    },
    date_update: {
        type: Sequelize.STRING
    },
    remarks: {
        type: Sequelize.STRING
    },
  },{
    freezeTableName: true,
    timestamps: false
  }
  
  );

  module.exports = tbl_user_groups;
