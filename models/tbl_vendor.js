const Sequelize = require('sequelize');
const connection = require('../config/database_globalwebapp');

const tbl_vendor = connection.define('tbl_vendor', {
    id_vendor: {
      type: Sequelize.STRING,
      primaryKey: true
    },
    vendor: {
        type: Sequelize.STRING,
    },
    alamat: {
        type: Sequelize.STRING,
    },
    npwp: {
        type: Sequelize.STRING,
    },
    notelp: {
        type: Sequelize.STRING,
    },
    email: {
        type: Sequelize.STRING,
    },
    fax: {
        type: Sequelize.STRING,
    },
    kode_pos: {
        type: Sequelize.STRING,
    },
    id_company: {
        type: Sequelize.STRING,
    },
    id_dept: {
        type: Sequelize.STRING,
    },
    status: {
        type: Sequelize.STRING,
    },
    tgl_input: {
        type: Sequelize.STRING,
    },
    create_by: {
        type: Sequelize.STRING,
    },
    contact_person: {
        type: Sequelize.STRING,
    },
    date_create: {
        type: Sequelize.STRING,
    },
    date_update: {
        type: Sequelize.STRING,
    },
    remarks: {
        type: Sequelize.STRING,
    },

  },{
    freezeTableName: true,
    timestamps: false
  }
  
  );

  module.exports = tbl_vendor;
