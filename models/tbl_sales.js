const Sequelize = require('sequelize');
const connection = require('../config/database_globalwebapp');


  const tbl_sales = connection.define('tbl_sales', {
    id_sales: {
      type: Sequelize.STRING,
      primaryKey: true
    },
    sal_name: {
        type: Sequelize.STRING
    },
    alamat: {
        type: Sequelize.STRING
    },
    phone: {
        type: Sequelize.STRING
    },
    branch_id: {
        type: Sequelize.STRING
    },
    status: {
        type: Sequelize.STRING
    },
  },{
    freezeTableName: true,
    timestamps: false
  }
  
  );

  module.exports = tbl_sales;
