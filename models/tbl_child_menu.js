const Sequelize = require('sequelize');
const connection = require('../config/database_globalwebapp');


  const tbl_child_menu = connection.define('tbl_child_menu', {
    id_child: {
      type: Sequelize.STRING,
      primaryKey: true
    },
    id_parent: {
        type: Sequelize.STRING
    },
    order_child: {
        type: Sequelize.STRING
    },
    desc_child: {
        type: Sequelize.STRING
    },
    anchor_child_url: {
        type: Sequelize.STRING
    },
    icon_child: {
        type: Sequelize.STRING
    },
    date_create: {
        type: Sequelize.STRING
    },
    date_update: {
        type: Sequelize.STRING
    },
    remarks: {
        type: Sequelize.STRING
    },
  },{
    freezeTableName: true,
    timestamps: false
  }
  
  );

  module.exports = tbl_child_menu;
