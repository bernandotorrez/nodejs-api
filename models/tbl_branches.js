const Sequelize = require('sequelize');
const connection = require('../config/database_globalwebapp');


  const tbl_branches = connection.define('tbl_branches', {
    branch_id: {
      type: Sequelize.STRING,
      primaryKey: true
    },
    id_company: {
        type: Sequelize.STRING
    },
    name_branch: {
      type: Sequelize.STRING
    },
    branch_short: {
        type: Sequelize.STRING
    },
    address: {
        type: Sequelize.STRING
    },
    city: {
        type: Sequelize.STRING
    },
    phone: {
        type: Sequelize.STRING
    },
    fax: {
        type: Sequelize.STRING
    },
    email: {
        type: Sequelize.STRING
    },
    status: {
        type: Sequelize.STRING
    },
    area_id: {
        type: Sequelize.STRING
    },
    place: {
        type: Sequelize.STRING
    },
    date_create: {
        type: Sequelize.STRING
    },
    date_update: {
        type: Sequelize.STRING
    },
    remarks: {
        type: Sequelize.STRING
    },
  },{
    freezeTableName: true,
    timestamps: false
  }
  
  );

  module.exports = tbl_branches;
