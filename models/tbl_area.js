const Sequelize = require('sequelize');
const connection = require('../config/database_globalwebapp');


  const tbl_area = connection.define('tbl_area', {
    area_id: {
      type: Sequelize.STRING,
      primaryKey: true
    },
    area_name: {
        type: Sequelize.STRING
    },
    status: {
        type: Sequelize.STRING
    },
  },{
    freezeTableName: true,
    timestamps: false
  }
  
  );

  module.exports = tbl_area;
