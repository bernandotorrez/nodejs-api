const Sequelize = require('sequelize');
const connection = require('../config/database_globalwebapp');

const tbl_counter_ppno = connection.define('tbl_counter_ppno', {
    id_dept: {
      type: Sequelize.STRING,
      primaryKey: true
    },
    counter: {
        type: Sequelize.STRING,
    },

  },{
    freezeTableName: true,
    timestamps: false
  }
  
  );

  module.exports = tbl_counter_ppno;
