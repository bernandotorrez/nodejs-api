const Sequelize = require('sequelize');
const connection = require('../config/database_globalwebapp');


  const tbl_brand = connection.define('tbl_brand', {
    id_brand: {
      type: Sequelize.STRING,
      primaryKey: true
    },
    brand_name: {
        type: Sequelize.STRING
    },
    type_veh: {
      type: Sequelize.STRING
    },
    status: {
        type: Sequelize.STRING
    },
    date_create: {
        type: Sequelize.STRING
    },
    date_update: {
        type: Sequelize.STRING
    },
    remarks: {
      type: Sequelize.STRING
    },
  },{
    freezeTableName: true,
    timestamps: false
  }
  
  );

  module.exports = tbl_brand;
