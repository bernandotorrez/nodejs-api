const Sequelize = require('sequelize');
const connection = require('../config/database_globalwebapp');

const tbl_coa = connection.define('tbl_coa', {
    id_coa: {
      type: Sequelize.STRING,
      primaryKey: true
    },
    nomor_coa: {
        type: Sequelize.STRING,
    },
    desc_coa: {
        type: Sequelize.STRING,
    },
    id_company: {
        type: Sequelize.STRING,
    },
    status: {
        type: Sequelize.STRING,
    },
    id_dept: {
        type: Sequelize.STRING,
    },
    id_divisi: {
        type: Sequelize.STRING,
    },
    date_create: {
        type: Sequelize.STRING,
    },
    date_update: {
        type: Sequelize.STRING,
    },
    remarks: {
        type: Sequelize.STRING,
    },

  },{
    freezeTableName: true,
    timestamps: false
  }
  
  );

  module.exports = tbl_coa;
