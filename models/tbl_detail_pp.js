const Sequelize = require('sequelize');
const connection = require('../config/database_globalwebapp');

const tbl_detail_pp = connection.define('tbl_detail_pp', {
    id_detail: {
      type: Sequelize.STRING,
      primaryKey: true
    },
    id_master: {
        type: Sequelize.STRING,
    },
    desc: {
        type: Sequelize.STRING,
    },
    spec: {
        type: Sequelize.STRING,
    },
    qty: {
        type: Sequelize.STRING,
    },
    harga: {
        type: Sequelize.STRING,
    },
    total: {
        type: Sequelize.STRING,
    },
    status: {
        type: Sequelize.STRING,
    },
    flagadjust: {
        type: Sequelize.STRING,
    },
    keterangan: {
        type: Sequelize.STRING,
    },
    po_reff: {
        type: Sequelize.STRING,
    },
    tax_type: {
        type: Sequelize.STRING,
    },
    tax_typepph: {
        type: Sequelize.STRING,
    },
    tax_detail: {
        type: Sequelize.STRING,
    },
    tax_detailpph: {
        type: Sequelize.STRING,
    },
    coa: {
        type: Sequelize.STRING,
    },
    no_act: {
        type: Sequelize.STRING,
    },
    date_balnce: {
        type: Sequelize.STRING,
    },

  },{
    freezeTableName: true,
    timestamps: false
  }
  
  );

  module.exports = tbl_detail_pp;
