const Sequelize = require('sequelize');
const connection = require('../config/database_globalwebapp');


  const tbl_parent_menu = connection.define('tbl_parent_menu', {
    id_parent: {
      type: Sequelize.STRING,
      primaryKey: true
    },
    order_parent: {
        type: Sequelize.STRING
    },
    parent_menu_desc: {
        type: Sequelize.STRING
    },
    parent_fix: {
        type: Sequelize.STRING
    },
    anchor_url: {
        type: Sequelize.STRING
    },
    icon: {
        type: Sequelize.STRING
    },
    status_del: {
        type: Sequelize.STRING
    },
    date_create: {
        type: Sequelize.STRING
    },
    date_update: {
        type: Sequelize.STRING
    },
    remarks: {
        type: Sequelize.STRING
    },
  },{
    freezeTableName: true,
    timestamps: false
  }
  
  );

  module.exports = tbl_parent_menu;
