const Sequelize = require('sequelize');
const connection = require('../config/database_globalwebapp');


  const tbl_menu_user_group = connection.define('tbl_menu_user_group', {
    id_menu_user_group: {
      type: Sequelize.STRING,
      primaryKey: true
    },
    id_group: {
        type: Sequelize.STRING
    },
    id_parent: {
        type: Sequelize.STRING
    },
    id_child: {
        type: Sequelize.STRING
    },
    id_subchild: {
        type: Sequelize.STRING
    },
    can_view: {
        type: Sequelize.STRING
    },
    can_add: {
        type: Sequelize.STRING
    },
    can_edit: {
        type: Sequelize.STRING
    },
    can_del: {
        type: Sequelize.STRING
    },
    status: {
        type: Sequelize.STRING
    },
    date_create: {
        type: Sequelize.STRING
    },
    date_update: {
        type: Sequelize.STRING
    },
    remarks: {
        type: Sequelize.STRING
    },
  },{
    freezeTableName: true,
    timestamps: false
  }
  
  );

  module.exports = tbl_menu_user_group;
