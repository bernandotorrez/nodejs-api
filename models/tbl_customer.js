const Sequelize = require('sequelize');
const connection = require('../config/database_globalwebapp');


  const tbl_customer = connection.define('tbl_customer', {
    id_cust: {
      type: Sequelize.STRING,
      primaryKey: true
    },
    cust_name: {
        type: Sequelize.STRING
    },
    jenkel: {
        type: Sequelize.STRING
    },
    religion: {
        type: Sequelize.STRING
    },
    ttg: {
        type: Sequelize.STRING
    },
    address: {
        type: Sequelize.STRING
    },
    phone: {
        type: Sequelize.STRING
    },
    phone2: {
        type: Sequelize.STRING
    },
    phone_office: {
        type: Sequelize.STRING
    },
    email: {
        type: Sequelize.STRING
    },
    branch_id: {
        type: Sequelize.STRING
    },
    status: {
        type: Sequelize.STRING
    },
  },{
    freezeTableName: true,
    timestamps: false
  }
  
  );

  module.exports = tbl_customer;
