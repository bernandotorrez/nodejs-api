const Sequelize = require('sequelize');
const connection = require('../config/database_globalwebapp');

const tbl_req_democar_group = connection.define('tbl_req_democar_group', {
    id_req_democar_group: {
      type: Sequelize.STRING,
      primaryKey: true
    },
    nama_konsumen: {
        type: Sequelize.STRING,
    },
    no_ktp: {
      type: Sequelize.STRING,
    },
    alamat: {
      type: Sequelize.STRING,
    },
    no_hp: {
      type: Sequelize.STRING,
    },
    stock_no: {
      type: Sequelize.STRING,
    },
    tujuan: {
      type: Sequelize.STRING,
    },
    kilo_berangkat: {
      type: Sequelize.STRING,
    },
    kilo_pulang: {
      type: Sequelize.STRING,
    },
    keterangan: {
      type: Sequelize.STRING,
    },
    nama_requester: {
      type: Sequelize.STRING,
    },
    jabatan_requester: {
      type: Sequelize.STRING,
    },
    sales_name: {
      type: Sequelize.STRING,
    },
    contact_sales: {
      type: Sequelize.STRING,
    },
    sign_req: {
      type: Sequelize.STRING,
    },
    sign_1: {
      type: Sequelize.STRING,
    },
    sign_2: {
      type: Sequelize.STRING,
    },
    status: {
      type: Sequelize.STRING,
    },
    send_approval: {
      type: Sequelize.STRING,
    },
    flag_print: {
      type: Sequelize.STRING,
    },
    submission_email: {
      type: Sequelize.STRING,
    },
    flag_status_req: {
      type: Sequelize.STRING,
    },

  },{
    freezeTableName: true,
    timestamps: false
  }
  
  );

  module.exports = tbl_req_democar_group;
