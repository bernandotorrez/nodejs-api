const Sequelize = require('sequelize');
const connection = require('../config/database_globalwebapp');

const tbl_date_compare = connection.define('tbl_date_compare', {
    id: {
      type: Sequelize.STRING,
      primaryKey: true
    },
    date_add: {
      type: Sequelize.STRING
    }

  },{
    freezeTableName: true,
    timestamps: false
  }
  
  );

  module.exports = tbl_date_compare;
