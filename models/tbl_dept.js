const Sequelize = require('sequelize');
const connection = require('../config/database_globalwebapp');


  const tbl_dept = connection.define('tbl_dept', {
    id_dept: {
      type: Sequelize.STRING,
      primaryKey: true
    },
    dept: {
        type: Sequelize.STRING
    },
    status: {
        type: Sequelize.STRING
    },
    flag_dept_inv_receipt: {
        type: Sequelize.STRING
    },
    date_create: {
        type: Sequelize.STRING
    },
    date_update: {
        type: Sequelize.STRING
    },
    remarks: {
        type: Sequelize.STRING
    },
  },{
    freezeTableName: true,
    timestamps: false
  }
  
  );

  module.exports = tbl_dept;
